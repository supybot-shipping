###
# supybot-shipping - A Supybot plugin to track shipments
# Copyright (C) 2008  Ian Weller <ianweller@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
###

import supybot.utils as utils
from supybot.commands import *
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks
import urllib
import re
from xml.dom import minidom




class Shipping(callbacks.Plugin):
    """Use "list shipping" to get the commands, then, for example, "ups
    TRACKINGNUMBER" to get the latest information."""

    _tracking_rss = "http://www.shaftek.org/code/track2rss/track2rss.pl?type"+\
            "=ups&tracking_number=%s"

    def __init__(self, irc):
        super(Shipping, self).__init__(irc)

    def ups(self, irc, msg, args, number):
        """<number>

        Returns the latest tracking information for UPS tracking number
        <number>."""
        rss = utils.web.getUrl(self._tracking_rss % number)
        dom = minidom.parseString(rss)
        item = dom.getElementsByTagName('item')[0]
        description = item.getElementsByTagName('description')[0]
        data = description.firstChild.data
        data = re.sub('\s*<br \/>\s*', '', data).strip()
        data = re.sub('\s\s+', ' ', data).strip().encode('utf-8')
        irc.reply(data)
    ups = wrap(ups, ['text'])


Class = Shipping


# vim:set shiftwidth=4 tabstop=4 expandtab textwidth=79:
